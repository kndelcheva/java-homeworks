package lecture07.task00;

import java.time.LocalDate;

public class SecondaryEducation {
    final String institutionName;
    final LocalDate enrollmentDate;
    LocalDate graduationDate;
    private double finalGrade;
    private boolean graduated;

    public SecondaryEducation(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate, double finalGrade) {
        this.institutionName = institutionName;
        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        setFinalGrade(finalGrade);
    }

    public void setFinalGrade(double grade) {
        if (grade > 0) {
            graduated = true;
            finalGrade = grade;
        } else {
            finalGrade = -1;
        }
    }

    public double getFinalGrade() {
        return finalGrade;
    }

    public boolean isGraduated() {
        return graduated;
    }
}
