package lecture06.task04;

public class Test {
    public static void main(String[] args) {
        Person obj1 = new Person();
        Person obj2 = new Person("Steven");
        Person obj3 = new Person("Angie", 32);

        System.out.println(obj1.getAge());
        System.out.println(obj1.getName());
        System.out.println(obj2.getName());

        System.out.println(obj1.introduce());
        System.out.println(obj2.introduce());
        System.out.println(obj3.introduce());
    }
}
