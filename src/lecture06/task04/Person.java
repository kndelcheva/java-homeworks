package lecture06.task04;

public class Person {
    String name;
    int age;

    Person() {
        this.name = "No name";
        this.age = -1;
    }

    Person(String name) {
        this.name = name;
        this.age = -1;
    }

    Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    void setAge(int age) {
        if (0 <= age && age <= 150) {
            this.age = age;
        } else {
            System.out.println("Not a valid age.");
        }
    }

    int getAge() {
        return age;
    }

    void setName(String name) {
        this.name = name;
    }

    String getName() {
        return name;
    }

    String introduce() {
        String nameToPrint = name;
        if (nameToPrint.equals("No name")) {
            nameToPrint = "John Doe";
        }

        if (age == -1) {
            return "Hello, I am " + nameToPrint + ".";
        } else {
            return "Hello, I am " + nameToPrint + ". I am " + age + " years old.";

        }
    }
}
