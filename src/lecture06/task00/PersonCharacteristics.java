package lecture06.task00;

import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();


        for (int i = 0; i < N; i++) {
            String dataLine = scanner.nextLine();
            String[] separatedData = dataLine.split(",");

            String firstName = separatedData[0];
            String lastName = separatedData[1];
            char gender = separatedData[2].charAt(0);
            int birthYear = Integer.parseInt(separatedData[3]);
            double weight = Double.parseDouble(separatedData[4]);
            int height = Integer.parseInt(separatedData[5]);
            String occupation = separatedData[6];

            double[] allGrades = new double[4];
            allGrades[0] = Double.parseDouble(separatedData[7]);
            allGrades[1] = Double.parseDouble(separatedData[8]);
            allGrades[2] = Double.parseDouble(separatedData[9]);
            allGrades[3] = Double.parseDouble(separatedData[10]);
            double averageGrade = (allGrades[0] + allGrades[1] + allGrades[2] + allGrades[3]) / 4;

            Person person = new Person(firstName, lastName, gender,birthYear, weight, height, occupation, allGrades);
            person.printPerson();
        }

    }
}

