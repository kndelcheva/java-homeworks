package lecture06.task01;

public class Printing {
    public static void main(String[] args) {
        printDetails("Stoyan", 29);
    }

     static void printDetails(String name, int age) {
         System.out.println(name + " is " + age + " years old." );
    }
}
