package lecture06.task08;

import java.util.Scanner;

public class VehicleMonitor {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        double f = scanner.nextDouble();
        double d = scanner.nextDouble();

        scanner.nextLine();

        Vehicle[] inputVehicles = new Vehicle[n];

        for (int i = 0; i < n; i++) {
            String line = scanner.nextLine();
            Vehicle vehicle = createVehiclefromInput(line);
            inputVehicles[i] = vehicle;
        }

        for (Vehicle vehicle : inputVehicles) {
            printVehiclesProperties(vehicle, f, d);
        }
    }

    private static Vehicle createVehiclefromInput(String input) {
        String[] vehicleInputFields = input.split(", ");
        String type = vehicleInputFields[0];
        String model = vehicleInputFields[1];
        int power = Integer.valueOf(vehicleInputFields[2]);
        double fuelConsumption = Double.valueOf(vehicleInputFields[3]);
        int yearProduced = Integer.valueOf(vehicleInputFields[4]);


        Vehicle vehicle = new Vehicle(type, model, power, fuelConsumption, yearProduced);

        if (vehicleInputFields.length >= 6) {
            vehicle.weight = Integer.valueOf(vehicleInputFields[5].trim());
        }
        if (vehicleInputFields.length >= 7) {
            vehicle.color = vehicleInputFields[6];
        }
        return vehicle;
    }

    private static void printVehiclesProperties(Vehicle output, double fuelPrice, double distance) {
        System.out.printf("%04d - %s, %d, %s%n", output.licenseNo, output.model, output.yearProduced, output.color);
        System.out.printf("Insurance cost: %.2f - Тravel cost: %.2f%n", output.getInsurancePrice(),
                output.calculateTripPrice(fuelPrice, distance));
    }

}



