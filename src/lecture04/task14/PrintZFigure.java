package lecture04.task14;

import java.util.Scanner;

public class PrintZFigure {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        for (int i = 0; i < n; i++) {
            System.out.print("* ");
        }

        for (int i = 0; i < n - 2; i++) {
            System.out.println();
            for (int j = 0; j < n * 2 - 1; j++) {

                if (j == (n * 2 - 1) - 3 - 2 * i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
        }
        System.out.println();

        for (int i = 0; i < n; i++) {
            System.out.print("* ");
        }
    }
}