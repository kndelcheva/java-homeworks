package lecture04.task10;

import java.util.Scanner;

public class PrintSumOfDigits {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        long number = scanner.nextLong();
        int sum = 0;

        while (number > 0) {
            long digit = number % 10;
            sum += digit;
            number /= 10;
        }
        System.out.println(sum);
    }
}
