package lecture04.task02;

import java.util.Scanner;

public class PrintNonDivisors {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int lastNumber = scanner.nextInt();

        for (int i = 0; i <= lastNumber; i++) {
            if (i % 3 != 0 && i % 7 != 0) {
                System.out.print(i + " ");
            }
        }
    }
}
