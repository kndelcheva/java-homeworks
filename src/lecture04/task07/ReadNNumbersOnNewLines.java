package lecture04.task07;

import java.util.Scanner;

public class ReadNNumbersOnNewLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        String allNumbers = "";

        for (int i = 0; i < number; i++) {
            int inputNumber = scanner.nextInt();
            allNumbers = allNumbers + inputNumber + " ";
        }
        System.out.println(allNumbers);
    }
}
