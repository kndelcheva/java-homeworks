package lecture04.task04;

import java.util.Scanner;

public class PrintMinAndMax {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        int min = scanner.nextInt();
        int max = min;

        for (int i = 1; i < n; i++) {
            int currentNumber = scanner.nextInt();

            if (currentNumber < min) {
                min = currentNumber;
            }
            if (currentNumber > max) {
                max = currentNumber;
            }
        }
        System.out.println(min + " " + max);
    }
}
