package lecture04.task01;

import java.util.Scanner;

public class PrintSumOf1ToN {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");

        double number = scanner.nextDouble();
        double sum = 0;

        do {
            sum += number;
            number--;
        } while (number != 0);
        System.out.print(sum);
    }
}
