package lecture04.task06;

import java.util.Scanner;

public class ReadNNumbersOnASingleLine {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            int inputNumber = scanner.nextInt();
            System.out.println(inputNumber);
        }
    }
}
