package lecture04.task12;

import java.util.Scanner;

public class PrintFirstDigit {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int number = scanner.nextInt();
        int digit = 0;
        while (number > 0) {
            digit = number % 10;
            number /= 10;
        }
        System.out.println(digit);
    }
}
