package lecture04.task11;

import java.util.Scanner;

public class PrintMirrorNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        long number = scanner.nextLong();

        while (number > 0) {
            long digit = number % 10;
            number /= 10;
            System.out.print(digit);
        }
    }
}
