package lecture04.task09;

import java.util.Scanner;

public class PrintOnlyEvenNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();

        for (int i = 0; i < n; i++) {
            int inputNumber = scanner.nextInt();
            if (inputNumber % 2 == 0) {
                System.out.print(inputNumber + " ");
            }
        }
    }
}
