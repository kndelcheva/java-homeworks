package lecture03.task03;

import java.util.Scanner;

public class InchesToCentimeters {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Please, enter a value in inches: ");
        float inch = scanner.nextFloat();

        float valueInCm = inch * 2.54f;

        System.out.printf("%.4f", valueInCm);
    }
}
