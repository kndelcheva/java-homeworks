package lecture03.task04;

import java.util.Scanner;

public class OddOrEven {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter a number: ");
        float number = scanner.nextFloat();
        int numberAsInt = (int) number;

        if (numberAsInt != number) {
            System.out.print("ERROR - Only integers can be ODD or EVEN!");
        } else {
            if (number % 2 == 0) {
                System.out.print("EVEN");
            } else {
                System.out.print("ODD");
            }
        }
    }
}
