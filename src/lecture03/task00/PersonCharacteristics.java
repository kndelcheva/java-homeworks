package lecture03.task00;

import java.util.Scanner;

public class PersonCharacteristics {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < N; i++) {
            String dataLine = scanner.nextLine();
            String[] separatedData = dataLine.split(";");

            String firstName = separatedData[0];
            String lastName = separatedData[1];
            char gender = separatedData[2].charAt(0);
            int birthYear = Integer.parseInt(separatedData[3]);
            int age = 2018 - birthYear;

            if (age < 0 || age > 130) {
                System.out.println("Error: Age not valid for contact " + (i + 1) + ". Year of birth must be between 1888 and 2018!");
                continue;
            }

            double weight = Double.parseDouble(separatedData[4]);

            if (weight <= 0) {
                System.out.println("Error: weight not valid for contact " + (i + 1) + ". Weight must be larger than 0!");
                continue;
            }
            int height = Integer.parseInt(separatedData[5]);

            if (height <= 0) {
                System.out.println("Error: height not valid for contact " + (i + 1) + ". Height must be larger than 0!");
                continue;
            }

            String occupation = separatedData[6];

            String vowelOrConsonant;
            switch (occupation.charAt(0)) {
                case 'A':
                case 'a':
                case 'E':
                case 'e':
                case 'I':
                case 'i':
                case 'O':
                case 'o':
                case 'U':
                case 'u':
                    vowelOrConsonant = "an";
                    break;

                default:
                    vowelOrConsonant = "a";
                    break;
            }

            double[] allGrades = new double[4];
            allGrades[0] = Double.parseDouble(separatedData[7]);
            allGrades[1] = Double.parseDouble(separatedData[8]);
            allGrades[2] = Double.parseDouble(separatedData[9]);
            allGrades[3] = Double.parseDouble(separatedData[10]);
            double averageGrade = (allGrades[0] + allGrades[1] + allGrades[2] + allGrades[3]) / 4;

            String possessivePronoun;
            String personalPronLowerCase;
            String personalPronUpperCase;

            if (gender == 'M') {
                possessivePronoun = "His";
                personalPronLowerCase = "he";
                personalPronUpperCase = "He";
            } else if (gender == 'F') {
                possessivePronoun = "Her";
                personalPronLowerCase = "she";
                personalPronUpperCase = "She";
            } else {
                System.out.println("Error! Wrong gender for contact " + (i + 1));
                continue;
            }


            System.out.printf("%s %s is %d years old. %s weight is %.1f kg and %s is %d cm tall.\n%s is %s %s with and average grade of" +
                            " %.3f.",
                    firstName, lastName, age, possessivePronoun, weight, personalPronLowerCase, height, personalPronUpperCase,
                    vowelOrConsonant, occupation, averageGrade);

            if (age < 18) {
                System.out.printf(" %s %s is under-aged.", firstName, lastName);
            }
            System.out.println();
        }
    }
}