package lecture03.task02;

import java.util.Scanner;

public class AdultChecker {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.print("Please, enter your age: ");
        int age = scanner.nextInt();

        boolean isAdult = age >= 18;

        if (age < 0) {
            System.out.println("error");
        } else {
            System.out.println(isAdult);
        }
    }
}