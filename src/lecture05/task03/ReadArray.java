package lecture05.task03;

import java.util.Scanner;

public class ReadArray {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number of values you want to input next: ");

        int n = sc.nextInt();
        int[] numbers = new int[n];

        System.out.println("Enter " + n + " numbers on the same line separated by space!");
        for (int i = 0; i < n - 1; i++) {
            int number = sc.nextInt();
            numbers[i] = number;
            System.out.print(number + ", ");
        }

        int number = sc.nextInt();
        numbers[n - 1] = number;
        System.out.println(numbers[n - 1]);
    }
}
