package lecture05.task11;

import java.util.Scanner;

public class PrintSplitIndex {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        int[] inputNumbers = new int[n];

        for (int i = 0; i < inputNumbers.length; i++) {
            inputNumbers[i] = scanner.nextInt();
        }
        int leftSum = inputNumbers[0];
        int rightSum = 0;

        for (int i = 1; i < inputNumbers.length; i++) {
            rightSum += inputNumbers[i];
        }

        if (leftSum == rightSum) {
            System.out.println(0);
            return;
        } else {
            for (int i = 1; i < inputNumbers.length; i++) {
                leftSum += inputNumbers[i];
                rightSum -= inputNumbers[i];
                if (leftSum == rightSum) {
                    System.out.println(i);
                    return;
                }
            }
        }
        System.out.println("NO");
    }
}
