package lecture05.task09;

import java.util.Arrays;
import java.util.Scanner;

public class PrintSortedNumbers {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] numbersToBeSorted = new int[n];

        for (int i = 0; i < n; i++) {
            numbersToBeSorted[i] = scanner.nextInt();
        }
        Arrays.sort(numbersToBeSorted);
        for (int arrayNumber : numbersToBeSorted) {
            System.out.print(arrayNumber + " ");
        }
    }
}
