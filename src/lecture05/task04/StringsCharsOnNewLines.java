package lecture05.task04;

import java.util.Scanner;

public class StringsCharsOnNewLines {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String line = sc.nextLine();

        for (char c : line.toCharArray()) {
            System.out.println(c);
        }
    }
}
