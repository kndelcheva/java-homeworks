package exam.forum;

public class UserNotAuthorException extends Throwable {

    UserNotAuthorException(String ex) {
        super("You are not author to change the topic");
    }
}
