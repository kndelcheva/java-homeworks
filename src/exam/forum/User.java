package exam.forum;

import java.time.LocalDateTime;

abstract class User {

    private String username;
    private String password;
    private LocalDateTime accountCreation;
    private String email;

    protected User(String name, String password, LocalDateTime accountCreation, String email) {
        this.username = name;
        this.password = password;
        this.accountCreation = accountCreation;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDateTime getAccountCreation() {
        return accountCreation;
    }

    public void setAccountCreation(LocalDateTime accountCreation) {
        this.accountCreation = accountCreation;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
