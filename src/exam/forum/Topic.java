package exam.forum;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Topic {

    private String name;
    private LocalDateTime topicCreationTime;
    private List<String> posts = new ArrayList<String>();
    private String topicAuthor;
    private String TopicCloser;
    private String LastPersonToModify;

    protected Topic(String name, LocalDateTime topicCreationTime, List<String> posts, String topicAuthor,
                    String topicCloser, String lastPersonToModify) {
        this.name = name;
        this.topicCreationTime = topicCreationTime;
        this.posts = posts;
        this.topicAuthor = topicAuthor;
        TopicCloser = topicCloser;
        LastPersonToModify = lastPersonToModify;
    }

    protected boolean addNewPost (String newPost) {
        return posts.add(newPost);
    }

    protected boolean topicClose() {
        return false;
    }

    protected String changeTopicName(String newName) {
        return this.name = newName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDateTime getTopicCreationTime() {
        return topicCreationTime;
    }

    public void setTopicCreationTime(LocalDateTime topicCreationTime) {
        this.topicCreationTime = topicCreationTime;
    }

    public List<String> getPosts() {
        return posts;
    }

    public void setPosts(List<String> posts) {
        this.posts = posts;
    }

    public String getTopicAuthor() {
        return topicAuthor;
    }

    public void setTopicAuthor(String topicAuthor) {
        this.topicAuthor = topicAuthor;
    }

    public String getTopicCloser() {
        return TopicCloser;
    }

    public void setTopicCloser(String topicCloser) {
        TopicCloser = topicCloser;
    }

    public String getLastPersonToModify() {
        return LastPersonToModify;
    }

    public void setLastPersonToModify(String lastPersonToModify) {
        LastPersonToModify = lastPersonToModify;
    }
}
