package exam.forum;

import java.time.LocalDateTime;

public class AdminUser extends User {

    protected AdminUser(String name, String password, LocalDateTime accountCreation, String email) {
        super(name, password, accountCreation, email);
    }
}
