package lecture10.task01;

public class NumberNotANumber {

    static boolean isNumber(String input) {

        try {
            Integer.parseInt(input);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
