package lecture10.task02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CredentialsTest {
    @Test
    void successful_input() throws Exception, PasswordMismatchException {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");

        // WHEN
        credentials.changePassword("1234", "xxxx");

        // THEN
        Assertions.assertTrue(credentials.matchingOldPassword("xxxx"));
    }

    @Test
    void error_when_changing_password_and_providing_mismatching_current() throws Exception {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");

        try {
            // WHEN
            credentials.changePassword("123444444", "xxxx");
            Assertions.fail("Method call did not throw an exception.");
        } catch (PasswordMismatchException ex) {
        }
    }

    @Test
    void error_when_changing_password_and_providing_the_current_one_as_new()
            throws Exception, PasswordMismatchException {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");

        try {
            // WHEN
            credentials.changePassword("1234", "1234");
            Assertions.fail("Method call did not throw an exception.");
        } catch (OldPasswordConflictException ex) {
            // THEN
            int index = ex.getPasswordConflictIndex();
            Assertions.assertEquals(0, index);
        }
    }

    @Test
    void error_when_changing_password_and_providing_an_old_one() throws Exception, PasswordMismatchException {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");
        credentials.changePassword("1234", "xxxx");

        try {
            // WHEN
            credentials.changePassword("xxxx", "1234");
            Assertions.fail("Method call did not throw an exception.");
        } catch (OldPasswordConflictException ex) {
            // THEN
            int index = ex.getPasswordConflictIndex();
            Assertions.assertEquals(1, index);
        }
    }
    @Test
    void when_changing_password_and_providing_the_current_one_as_new_return_0()
            throws Exception, PasswordMismatchException {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");
        String usedPassword = "1234";
        String[] passwords = new String[]{"5678", "9012", "3456", "7890"};
        for (String password : passwords) {
            credentials.changePassword(usedPassword, password);
            usedPassword = password;
        }

        try {
            // WHEN
            credentials.changePassword("7890", "7890");
            Assertions.fail("Method call did not throw an exception.");
        } catch (OldPasswordConflictException ex) {
            // THEN
            int index = ex.getPasswordConflictIndex();
            Assertions.assertEquals(0, index);
        }
    }
    @Test
    void when_changing_password_and_providing_an_older_one_as_new_return_the_distance_between_them()
            throws Exception, PasswordMismatchException {

        // GIVEN
        Credentials credentials = new Credentials("user", "1234");
        String usedPassword = "1234";
        String[] passwords = new String[]{"5678", "9012", "3456", "7890", "1122", "2233", "4455", "8899"};
        for (String password : passwords) {
            credentials.changePassword(usedPassword, password);
            usedPassword = password;
        }

        try {
            // WHEN
            credentials.changePassword("8899", "3456");
            Assertions.fail("Method call did not throw an exception.");
        } catch (OldPasswordConflictException ex) {
            // THEN
            int index = ex.getPasswordConflictIndex();
            Assertions.assertEquals(5, index);
        }
    }
}