package lecture10.task02;

public class OldPasswordConflictException extends Exception {


    private final int samePasswordAtIndex;

    public OldPasswordConflictException(int index) {
        samePasswordAtIndex = index;
    }

    int getPasswordConflictIndex() {
        return samePasswordAtIndex;
    }
}
