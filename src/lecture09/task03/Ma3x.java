package lecture09.task03;

public class Ma3x {

    double pricePerHour;

    Ma3x(double pricePerHour) {
        this.pricePerHour = pricePerHour;
    }

    double charge(Customer customer, int hours) {

        double price = hours * pricePerHour;
        double totalPrice = customer.calculateDiscountedPrice(price);
        return totalPrice;
    }

    public static void main(String[] args) {
        PlatinumVip superClient = new PlatinumVip();
        Ma3x testObject = new Ma3x(2);

        System.out.println(testObject.charge(superClient, 1));

    }
}

