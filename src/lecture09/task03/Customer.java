package lecture09.task03;

public abstract class Customer {
    private double discount;

    public Customer(double discount) {
        this.discount = discount;
    }

    double getDiscount() {
        return discount;
    }
    double calculateDiscountedPrice(double price) {
        return  price * (1 - discount);
    }

}
