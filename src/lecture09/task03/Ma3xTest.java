package lecture09.task03;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class Ma3xTest {

    @Test
    void silver_customer_has_10_percent_discount() {
// GIVEN
        Ma3x ma3x = new Ma3x(2);
        SilverVip customer = new SilverVip();
// WHEN
        double price = ma3x.charge(customer, 2);
//THEN
        Assertions.assertEquals(3.6, price);
    }

    @Test
    void novip_customer_has_no_discount() {
        //GIVEN
        Ma3x testClient = new Ma3x(2);
        NoVip customer = new NoVip();

        //WHEN
        double price = testClient.charge(customer, 2);

        //THEN
        Assertions.assertEquals(4.0, price);


    }


}