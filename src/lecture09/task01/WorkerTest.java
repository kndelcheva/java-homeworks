package lecture09.task01;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class WorkerTest {
    @Test
    void assert_correct_fields_are_populated_for_worker_info() {
// GIVEN
        String firstName = "Angel";
        String lastName = "Enchev";
        double weekSalary = 400;
        Person worker = new Worker(firstName, lastName, weekSalary, 8);
// WHEN
        String info = worker.getInfo();
// THEN
        Assertions.assertTrue(info.contains("First name: " + firstName));
        Assertions.assertTrue(info.contains("Last name: " + lastName));
        Assertions.assertTrue(info.contains("Occupation: Worker"));
        Assertions.assertTrue(info.contains("Week salary: " + weekSalary));
        Assertions.assertFalse(info.contains("Faculty number:"));
        Assertions.assertFalse(info.contains("Hours per day"));
    }

    @Test
    void worker_salary_per_hour_calculated_properly() {
// GIVEN
        double weekSalary = 400;
        int workHoursPerDay = 8;
        Person worker = new Worker("Harry", "Evans", 400, 8);
// WHEN
        String info = worker.getInfo();
// THEN
        Assertions.assertTrue(info.contains("Salary per hour: 10.00"));
    }
}