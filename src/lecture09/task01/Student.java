package lecture09.task01;

public class Student extends Person {


    String facultyNumber;
    int lectureCount;
    int exerciseCount;

    public Student(String firstName, String lastName, String facultyNumber, int lectureCount, int exerciseCount) {
        super(firstName, lastName);
        this.facultyNumber = facultyNumber;
        this.lectureCount = lectureCount;
        this.exerciseCount = exerciseCount;
    }

    @Override
    String getInfo() {
        String baseInfo = super.getInfo();
        return String.format(baseInfo + "Occupation: Student%n" +
                        "Faculty number: %s%n" +
                        "Hours per day: %f%n",
                facultyNumber, getHoursPerDay());
    }

    private double getHoursPerDay() {
        return (2 * lectureCount + 1.5 * exerciseCount) / 5;
    }
}
