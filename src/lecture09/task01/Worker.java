package lecture09.task01;

public class Worker extends Person {

    double weekSalary;
    int workHoursPerDay;

    public Worker(String firstName, String lastName, double weekSalary, int workHoursPerDay) {
        super(firstName, lastName);
        this.weekSalary = weekSalary;
        this.workHoursPerDay = workHoursPerDay;
    }
    @Override
    String getInfo() {
        String baseInfo = super.getInfo();
        return String.format(baseInfo + "Occupation: Worker%n" +
                        "Week salary: %f%n" +
                        "Salary per hour: %f%n",
                weekSalary, getSalaryPerHour());
    }
    private double getSalaryPerHour() {
        return (weekSalary / 5) / workHoursPerDay;
    }
}

