package lecture09.task00;

import lecture09.task00.education.HigherEducation;
import lecture09.task00.education.PrimaryEducation;
import lecture09.task00.education.SecondaryEducation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class PersonCharacteristics {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int N = scanner.nextInt();
        scanner.nextLine();


        for (int i = 0; i < N; i++) {
            String dataLine = scanner.nextLine();
            String[] separatedData = dataLine.split(";");

            String firstName = separatedData[0];
            String lastName = separatedData[1];
            char gender = separatedData[2].charAt(0);
            int height = Integer.parseInt(separatedData[3]);
            LocalDate dateOfBirth = LocalDate.parse(separatedData[4], DateTimeFormatter.ofPattern("d.M.yyyy"));
            char educationType = separatedData[5].charAt(0);
            String institutionName = separatedData[6];
            LocalDate enrollmentDate = LocalDate.parse(separatedData[7], DateTimeFormatter.ofPattern("d.M.yyyy"));
            LocalDate graduationDate = LocalDate.parse(separatedData[8], DateTimeFormatter.ofPattern("d.M.yyyy"));
            double finalGrade = -1;
            if (separatedData.length > 9) {
                finalGrade = Double.parseDouble(separatedData[9]);
            }

            Person person;
            switch (educationType) {
                case 'P':
                    PrimaryEducation pEducation = new PrimaryEducation(institutionName, enrollmentDate, graduationDate);
                    person = new Person(firstName, lastName, gender, height, dateOfBirth, pEducation);
                    break;
                case 'S':
                    SecondaryEducation sEducation = new SecondaryEducation(institutionName, enrollmentDate, graduationDate);
                    person = new Person(firstName, lastName, gender, height, dateOfBirth, sEducation);
                    sEducation.graduate(finalGrade);
                    break;
                case 'B':
                case 'M':
                case 'D':
                    HigherEducation hEducation = new HigherEducation(institutionName, enrollmentDate, graduationDate);
                    person = new Person(firstName, lastName, gender, height, dateOfBirth, hEducation);
                    hEducation.graduate(finalGrade);
                    break;
                default:
                    throw new IllegalArgumentException("Wrong education type.");
            }
            person.printPerson();
        }
    }
}


