package lecture09.task04;

public abstract class Dwarf extends Character {
    public Dwarf(String name) {
        super(name, 5, 20);
    }
}
