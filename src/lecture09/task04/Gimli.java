package lecture09.task04;

public class Gimli extends Dwarf {

    public Gimli() {
        super("Gimli");
    }

    @Override
    void specialAbility() {
        super.healFor(5);
    }
}
