package lecture09.task04;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class BattlefieldTest {

    @Test
    void gimli_fights_six_orcs_and_is_victorious() {
// GIVEN
        Gimli gimli = new Gimli();
        Orc[] orcs = new Orc[] {
                new OrcFighter("Golfimbul"),
                new OrcFighter("Golfimbul2"),
                new OrcFighter("Golfimbul3"),
                new OrcFighter("Golfimbul4"),
                new OrcFighter("Golfimbul5"),
                new OrcFighter("Golfimbul6")
        };
        Battlefield battlefield = new Battlefield(gimli, orcs);
// WHEN
        String outcome = battlefield.combat();
// THEN
        Assertions.assertEquals(battlefield.victoryMessage(), outcome);
    }
    @Test
    void gimli_fights_seven_orcs_and_is_defeated() {
// GIVEN
        Gimli gimli = new Gimli();
        Orc[] orcs = new Orc[] {
                new OrcFighter("Golfimbul"),
                new OrcFighter("Golfimbul2"),
                new OrcFighter("Golfimbul3"),
                new OrcFighter("Golfimbul4"),
                new OrcFighter("Golfimbul5"),
                new OrcFighter("Golfimbul6"),
                new OrcFighter("Golfimbul7")
        };
        Battlefield battlefield = new Battlefield(gimli, orcs);
// WHEN
        String outcome = battlefield.combat();
// THEN
        Assertions.assertEquals(battlefield.defeatMessage("Golfimbul7"), outcome);
    }
}


