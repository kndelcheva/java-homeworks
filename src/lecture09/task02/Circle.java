package lecture09.task02;

public class Circle extends Shape {
    double r;

    public Circle(double r) {
        this.r = r;
    }

    @Override
    double getArea() {
        double circleArea = (Math.PI) * (Math.pow(r, 2));
        return circleArea;
    }

}
