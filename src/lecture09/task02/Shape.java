package lecture09.task02;

public abstract class Shape {

    abstract double getArea();
}
