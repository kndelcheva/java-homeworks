package lecture09.task02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RectangleTest {

    @Test
    void calculate_area_of_rectangle() {

        //GIVEN
        Shape rectangle = new Rectangle(4, 5);

        //WHEN
        double rectangleArea = rectangle.getArea();

        //THEN
        Assertions.assertEquals(20, rectangleArea);
    }

}