package lecture09.task02;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SquareTest {

    @Test

    void calculate_area_of_square() {

        //GIVEN
        Shape square = new Square(5);

        //WHEN
        double area = square.getArea();

        //THEN
        Assertions.assertEquals(25, area);
    }
}