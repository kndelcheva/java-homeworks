package lecture09.task02;

public class Square extends Shape {
    double a;

    public Square (double a) {
        this.a = a;
    }
    @Override
    double getArea() {
        double squareArea = Math.pow(a, 2);
        return squareArea;
    }
}
